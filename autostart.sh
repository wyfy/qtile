#!/bin/bash
#this is the display configuration

xrandr --output DisplayPort-0 --primary --mode 3840x2160 --pos 1080x0 --rotate normal --output DisplayPort-1 --mode 1920x1080 --pos 0x0 --rotate left --output HDMI-A-0 --off --output HDMI-A-1 --off

picom --experimental-backends vsync = true -b

nitrogen --restore
